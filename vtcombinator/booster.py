import sys
import os

import socket
def str_append_list_join(s, n):
    l1 = []
    i = 0
    while i < n:
        l1.append(s)
        i += 1
    return ''.join(l1)

class gcomponent:
    def __init__(self, l, t, pc=None, cn=None,fn=None):
        self.module = []
        self.pin_count = pc
        self.component_name = cn
        self.function = fn
        self.hierarchy = []
        self.levels = l
        self.toplevel = t

    def make_levels(self):
        if self.component_name is None:
            return
        else:
            print("valid component found.")
            if self.toplevel:
                print("top level module found.")
                print(f"pin count: {self.pin_count}")

    def add_to_hierarchy(self, pc):
        if pc is None:
            print("error: null part specified.")
        self.hierarchy.append(pc)

    def get_level(self, component):
        return self.levels - component.levels

    def list_parts_in_hierarchy(self):
        print(f" === hierarchy for {self.component_name} ===")
        for f in self.hierarchy:
            if f.component_name is None:
                print("unnamed component.")
            g = self.get_level(f)
            print(f"{g} {str_append_list_join('-', g)}  {f.component_name}")






test = gcomponent(l=10, t=True, pc=6200, cn="Testbench")
bb = gcomponent(l=9, t=False, pc=100, cn="TestComponent_1")
test.make_levels()
bb.make_levels()

test.add_to_hierarchy(bb)
test.list_parts_in_hierarchy()

# print(str_append_list_join('-',3))
# print(str_append_list_join('-',4))
# print(str_append_list_join('-',1))
