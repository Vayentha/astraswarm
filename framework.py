
from bottle import Bottle, route, run, template, response
from bottle import static_file
import hashlib
import importlib

cyanogenmod = importlib.import_module('cyanogen')

app = Bottle()
llist = cyanogenmod.SLinkedList()

if cyanogenmod == None:
    print("Error: Cyanogen module could not be loaded.")
else:
    llist.AtEnd("Cyanogen Mod installed.")

@app.route('/')
def home():
    return "Welcome to VayenthaNet"

@app.route('/astra/<name>')
def homepage(name='Vayentha'):
    return template('homepage', name=name, rank = 12)

@app.route('/download')
def download(filename):
    return

#@app.route('/modules')
#def modules(list):
#    return template('modlist', list=list)

@app.route('/cyanogen')
def frontend_cyanogen():
    return cyanogenmod.cyanogen_test_import()   # should load successfully!

#@app.route('/base')
#def base(list=llist.LListprint()):
#    return template('<h3>{{list}}</h3>')

@app.route('/farsi')
def farsi():
    return 'فرزیسص'


app.run(host='127.0.0.1', port=8080)
