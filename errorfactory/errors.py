import asyncio

class generalError:
    def __init__(self, ec):
        self.errorName = "Unspecified General Error"
        self.errorCode = ec
        self.attachedModules = []

    def attach_error_module(self, emod_str):
        self.attachedModules.append(emod_str)

# system errors between 2100 and 2199
class systemError(generalError):
    def __init__(self, code, name, module):
        self.errorName = name
        self.errorCode = code
        self.attachedModules = []
        self.attachedModules.append(module)

    def system_error(self):
        self.errorCode = self.errorCode + 1

    def unspecifiedHWSE(self):
        self.errorCode = self.errorCode + 2

    def append_module(self, mod):
        for m in self.attachedModules:
            if m == mod:
                return
            self.attachedModules.append(mod)

    def getEC(self):
        return (self.errorCode, self.errorName, self.attachedModules)

    def check_gitlab_repos(self):
        url = f'https://gitlab.com/Vayentha/{self.attachedModules[0]}'
        print(url)

    def cull_bad_imports(self):
        exists = self.check_gitlab_repos()
        if exists:
            return
        else:
            self.attachedModules.remove(self.attachedModules[0])








# se = systemError("unspecifiedSYSERROR", "vayenthamod1")

#print(se.getEC())
#se.check_gitlab_repos()
#yoc = se.getEC()
#print(yoc[0])
#print(yoc[1])
#print(yoc[2])