import numpy
from vtcombinator import booster
import os
import asyncio
import time
import socket
from errorfactory import errors
from bottle import route, run, template

estub = 'riscv-opcode-sys'

module='riscv-compiletime-subsystem'

error_init = errors.systemError(code= 2300, name=estub, module=module)

error_init.append_module(module)

error_init.append_module('vayentha-core')

# error_init.append_module('riscv-compiletime-subsystem')

print(error_init.getEC())

opcode_types = ["R", "I", "S", "B", "U", "J"]    # six types

path_mil = "milcom/"
path_civ = "civcom/"

# check RISCV on the net wiki
# used http://www-inst.eecs.berkeley.edu/~cs152/sp19/handouts/sp19/riscv-spec-rvv-v0p4.pdf pg 136 for reference
class rv64_opcode:
    def __init__(self, type, name=None):
        if name is None:
            self.name = "UNDEFINED"
        else:
            self.name = name
        self.package = "RV64"
        self.cmdlength=64
        if type not in opcode_types:
            self.type=opcode_types[0]
            return
        self.type = type
        self.opc1 = ""
        self.opc2 = ""      # break into sixteen bit chunks

    def generate_valid_schema_tag(self):
        if self.type == "R":
            print("R-TYPE")
        if self.type == "I":
            print("I-TYPE")
        if self.type == "S":
            print("S-TYPE")
        if self.type == "B":
            print("B-TYPE")
        if self.type == "U":
            print("U-TYPE")
        if self.type == "J":
            print("J-TYPE")

    def opc_listing(self):
        print_opc()
        print(self.package)
        print(self.type)
        print(self.name)
        self.generate_valid_schema_tag()    # see riscv types

    def opc_logic_encode(self):
        return

    def validate_logic(self):
        return

    async def get_build_conf(self):
        print(self.package)

    async def wtf(self):
        print("gaygaygay")

    async def set_build_path(self):
        ip = "172.0.0.1"



def print_opc():
    print("========")

async def main():
    rv64 = rv64_opcode("I")
    bad64 = rv64_opcode("GGG")
    await asyncio.sleep(0.1)
    # await asyncio.sleep(5)
    rv64.opc_listing()
    # print(await get_build_conf(rv64))
    task = asyncio.create_task(rv64.get_build_conf())
    bad64.opc_listing()
    await asyncio.gather(       # use gather to bundle functions together
        task,
        #rv64.wtf()
    )
    # print(task)

asyncio.run(main())

@route('/indexer/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/core/<name>')
def core(name):
    return template('homepage', name=name, rank=4)

run(host='localhost', port=8080)