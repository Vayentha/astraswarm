import os
import sys
from datetime import date
import numpy as np
import namegenerator
import names
from ssh2.session import Session
import socket
import urllib.request

# from pycurl import
# import cyanogen


def systest():
    print("mwd characters package loaded.")

def generate_rand_name(num):
    for i in range (0,num):
        print(f"{names.get_full_name()}\n")

def parse_cli_args():
    n = len(sys.argv)
    if n == 0 or n==1:
        print("no CLI args")
        return
    print(f"\n total arguments passed: {n}\n")
    print(f"script name: {sys.argv[0]}")
    print("arguments passed: ", end = " ")
    for i in range(1,n):
        print(sys.argv[i], end = " ")
    print()


def gen_rand_trash_characters(num):
    for i in range(0,num):
        nchar = MWDCharacter(names.get_full_name(), "1969-04-20", 4, 25, False)
        char_formatting_stub()
        nchar.print_character_stats()
        ending_stub()

def cli_mode():
    print("CLI mode engaged")


def char_formatting_stub():
    print("--- Character info ---\n")


def ending_stub():
    print("===-----==0v0==-----===\n")


def race_stub():
    print("--- Race Info: ---")

def block_delimeter():
    print("======= BLOCK DELIMETER =======\n")


# class for technology. extend this for items
class Tech:
    def __init__(self, tech_rank):
        self.tech_rank = tech_rank


# class for races. We all know which one is the worst.
class Race:
    def __init__(self, race_type, num_pops):
        self.race_type = race_type
        self.num_pops = num_pops
        self.race_rank = 0

    def race_rank_adjust(self, rank=None):
        if rank is None:
            self.race_rank = self.race_rank + 1
        else:
            self.race_rank = self.race_rank + rank

    def print_race_stats(self):
        race_stub()
        print(f"{self.race_type}")
        print(f"{self.race_rank}")
        print(f"total pops: {self.num_pops}\n")


class NPC:
    def __init__(self, name):
        self.name = name


class MWDCharacter:
    def __init__(self, name, dob, rank, level, magic):
        self.name = name
        self.dob = date.fromisoformat(dob)
        self.rank = rank
        self.level = level
        self.magic = magic
        self.married = False
        self.issue = []
        self.issue_arr = []
        self.friends = []

    def print_character_stats(self):
        print(f"{self.name}\n")
        print(f"{self.dob}\n")
        print(f"{self.rank}\n")
        print(f"{self.level}\n")
        print(f"marriage status: {self.married}\n")
        if self.magic == True:
            print("character can use magic.")
        else:
            print("character is a muggle.")

    def generate_issue(self, issue_name):
        if self.married == False:
            return
        else:
            self.issue.append(issue_name)
            # generate something else here
        return

    def divorce(self):
        if self.married == True:
            self.married = False
        else:
            return

    def create_friendship(self, friend):
        return

    def add_marriage(self):
        if self.married == False:
            self.married = True
        elif self.married == True:
            self.married = False


class MainCharacter(MWDCharacter):
    def __init__(self, name, dob, rank, level, magic, current_arc, experience, starting_money):
        self.name = name
        self.dob = date.fromisoformat(dob)
        self.rank = rank
        self.level = level
        self.magic = magic
        self.married = False
        self.issue = []
        if current_arc is None:
            current_arc = "story arc will be set later"
        else:
            self.current_arc = current_arc
        self.experience = experience
        self.money = starting_money
        self.alive = True


class StoryGen:
    def __init__(self, num_nodes):
        self.nodes = num_nodes

    def find_probable_events(self):
        return

    def refactor_guarantor_events(self):
        return

    def find_webhook(self):
        return


class Ardashir:
    """socket connect class"""
    def __init__(self, sock=None):
        if sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock

    # connect to host downstairs and use port 80 (default for ssh)
    def rune_connect(self, host, port):
        self.sock.connect((host, port))

    def ssh2_fport(self, host, port):
        self.rune_connect(self, host, port)
        session = Session()
        session.handshake(self.sock)


def get_pipeline_config():
    host = 'http://gitlab.com/Vayentha/astraswarm/pipelines'
    return




#Nala = MWDCharacter("Nala", "1998-02-04", 12, 85, True)

#Nala.print_character_stats()

#Nala.add_marriage()

#Nala.print_character_stats()


#generate_rand_name(5)

#gen_rand_trash_characters(3)


#jew = Race("Judaism", 1233333)

#jew.print_race_stats()

#jew.race_rank_adjust()

#jew.print_race_stats()

#jew.race_rank_adjust(5)

#jew.print_race_stats()

# testing command line args
#parse_cli_args()

with urllib.request.urlopen('http://python.org') as response:
    html = response.read()
    print(html)

