import json
import asyncio
from vtcombinator import booster    # for classification of parts
import os


async def get_json(file_t):
    with open(file_t) as json_file:
        print(f"reading from rune {file_t}")
        data = json.load(json_file)
        for r in data['rune']:
            print(f"Rune number {r['rune_num']}")
            print(f"Rune name: {r['name']}")
            namevar = r['name']
            print(f"Built On: {r['website']}")
            websitevar = r['website']   # set these to make sure data is in scope
        for j in data['rune_meta']:
            print(f"========META for {namevar}========")
            print(f"File Location: {j['file_location']}")
            print(f"pipeline ID: {j['pipeline']}")

# custom or standard opcodes can be encoded into json files
async def opcode_json(file_t):
    with open(file_t) as opc_json:
        print(f"reading from opcode rune: {file_t}")
        data = json.load(opc_json)
        if data is None:
            print("error! null json file!")
        for o in data['opc_manifest']:
            if(o['opcode_t'] is None):
                print("no valid opcode found.")
            print(f"{booster.str_append_list_join('-',2)} opcodes: {o['opcode_t']}")
    return

async def kickoff_build(data):
    return


def export_format():
    print("------------")
    print(os.listdir('scripts/'))
    print("------------")


async def main():
    await(get_json('runetest.json'))
    await(opcode_json('runetest.json'))
    export_format()

asyncio.run(main())