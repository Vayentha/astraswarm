import os
import sys
import yfinance as yf
import namegenerator as ng
import hashlib as hl
from money import Money

ticker_pool = ['GOOG', 'MSFT']

second_pool = []

third_pool = []

fourth_pool = []

fifth_pool = []

sixth_pool = []

def stock_stub(name):
    print(f"-----{name}-----")

def endln_stub():
    print("-----------------")

def test_money():
    print("money test")
    m = Money(amount=100000, currency='USD')
    print(m)

def yft_overlord():
    print("yfinance engine started. Waiting on network...")
    stock_stub(name="ticker_pool")
    print(ticker_pool)
    endln_stub()
    stock_stub(name="second_pool")
    print(second_pool)
    endln_stub()

def gen_investors():
    return


# goal is to simulate a lot of investors and decide which ones are best
# jews did 9/11
class Investor:
    def __init__(self, name, starting_currency):
        self.tlist = []
        self.name = name
        self.starting_currency = starting_currency
        self.money = Money(amount=starting_currency, currency='USD')
        self.good_stock_pool = []
        self.bad_stock_pool = []
        self.stable_stock_pool = []

    # see which stocks are doing well
    def get_valuable_stocks(self):
        return

    # see which stocks are doing poorly
    def get_bad_stocks(self):
        return

    #calculate monetary changes
    def calculate_change(self):
        return

    # add a stock to the ticker list
    def add_to_tlist(self, ti):
        self.tlist.append(ti)
        print(f"appended {ti} to list")

    # refactor good stocks from bad stocks
    def refactor_stocks(self):
        return


class StockPool:
    def __init__(self):
        self.tech_stock = []
        self.pharma_stock = []
        self.finstock = []
        self.minag_stock = []
        self.risk_pool = []
        self.safe_pool = []
        self.contra_pool = []

    def add_tech(self, techstock):
        self.tech_stock.append(techstock)

    def add_pharma(self, pharmastock):
        self.pharma_stock.append(pharmastock)

    def add_finstock(self, finstock):
        self.finstock.append(finstock)

    def add_minag_stock(self, minagstock):
        self.minag_stock.append(minagstock)

    def add_risk_stock(self, risk):
        self.risk_pool.append(risk)

    def add_safe_stock(self, safe):
        self.safe_pool.append(safe)

    def add_contra_stock(self, contra):
        self.contra_pool.append(contra)


def generate_sample_portfolio():
    return



yft_overlord()


test_money()

