import os
import sys
import numpy as np
import importlib
from anytree import Node, RenderTree
import yfinance as yf

importlib.import_module("characters") # note: executes scripts here

class GenericGovt:
    def __init__(self, gov_type, num_employees):
        self.gov_type = gov_type
        self.num_employees = num_employees
        self.employees = []
        self.toplevel = Node("Tlevel")

    def generate_govt_structure(self):
        return


class CombineGovt(GenericGovt):
    def __init__(self, gov_type, num_employees, combine_levels):
        self.gov_type = gov_type
        self.num_employees = num_employees
        self.combine_levels = combine_levels
        self.employees = []
        self.toplevel = Node("Tlevel_combine")

    def generate_govt_structure(self):
        print(f"combine levels: {self.combine_levels}")
        print(RenderTree(self.toplevel))
        return


class TerranGovernment(GenericGovt):
    def __init__(self):
        super




def yftest():
    ticker = 'MSFT'
    ticker_data = yf.Ticker(ticker)
    tickerDF = ticker_data.history(period='1d', start='2000-01-01', end='2019-01-01')
    print(tickerDF)


def get_broad_history(ticker):
    print(f"\nticker: {ticker}\n")
    ticker_data = yf.Ticker(ticker)
    # yf.ticker.Ticker.get_recommendations()
    tickerDF = ticker_data.history(period='1d', start='2000-01-01', end='2019-01-01')
    print(tickerDF)


def find_high_point():
    return

def find_low_point():
    return

def get_likely_flex_value():
    return

def calculate_worst_case():
    return

def generate_stocks():
    return

class GoodStockTree:
    def __init__(self, toplevel):
        get_broad_history(toplevel)
        top = Node(toplevel)
        return


print("reached rcombinator")
# yftest()

get_broad_history('GOOG')