import numpy as np
import os
import sys
import ctypes
import  hashlib as hl

def test_np():
    return


class BDComparator:
    def __init__(self, id, item1, item2):
        self.id = id
        self.item1 = item1
        self.item2 = item2
        self.context = ComparatorObj(item=item1)

    def general_compare(self):
        return


class ComparatorObj:
    def __init__(self, item):
        self.item = item


class ConverterRune:
    def __init__(self):
        return


class ConfluxRune:
    def __init__(self):
        return


class BlastingRune(ConfluxRune):
    def __init__(self):
        return


class CombinerRune(ConfluxRune):
    def __init__(self):
        return

def generate_padding():
    print("hashes are:")


class VorceriKey:
    def __init__(self, first_dat, second_dat, third_dat):
        self.hash1 = hl.sha3_512(bytes(first_dat, 'utf-8'))
        self.hash2 = hl.sha3_512(bytes(second_dat, 'utf-8'))
        self.hash3 = hl.sha3_512(bytes(third_dat, 'utf-8'))
        self.combine = True
        self.symbiote1 = []
        self.symbiote2 = []
        self.symbiote3 = []
        # self.ghash = str(self.hash1.hexdigest()) ^ str(self.hash2.hexdigest())

    def print_hashes(self):
        generate_padding()
        print(self.hash1.hexdigest())
        print(self.hash2.hexdigest())
        print(self.hash3.hexdigest())
        # print(self.ghash)

    def generate_symbiotic_hashes(self):
        self.symbiote1.append(self.hash1.hexdigest())
        self.symbiote1.append(self.hash2.hexdigest())
        self.symbiote2.append(self.hash2.hexdigest())
        self.symbiote2.append(self.hash3.hexdigest())
        self.symbiote3.append(self.hash1.hexdigest())
        self.symbiote3.append(self.hash3.hexdigest())

    def print_symbiotes(self):
        print("symbiotes are:")
        print(self.symbiote1)
        print(self.symbiote2)
        print(self.symbiote3)
        return


class Exchange:
    def __init__(self):
        self.key_order = 0
        self.transform = []
        self.macro = []
        self.init_vector = np.random.random(7)
        for i in range(0,3):
            self.transform.append(self.init_vector[i])

        self.pivot = self.init_vector[4]
        for i in range(4,7):
            self.macro.append(self.init_vector[i])
        return

    def get_macro(self):
        print("macro is:")
        print(self.macro)

    def get_transform(self):
        print("transform is:")
        print(self.transform)

    def get_init_vector(self):
        print(f"initialization vector:")
        print(self.init_vector)

    def force_swap(self):
        return

# secure exchange platform
class SecureExchange(VorceriKey, Exchange):
    def __init__(self, data):
        self.name = "secure exchange"
        self.vkey = VorceriKey(data[0], data[1], data[2])
        self.exch = Exchange()
        self.exch.get_init_vector()
        self.exch.get_transform()
        self.exch.get_macro()
        if self.vkey.hash1 is None or self.vkey.hash2 is None or self.vkey.hash3 is None:
            return
        else:
            self.vkey.print_hashes()


    def secure_swap(self):
        return


#vor = VorceriKey('val1', 'val2', 'val3')

#vor.generate_symbiotic_hashes()

#vor.print_symbiotes()

#vor.print_hashes()

#sec = SecureExchange(['val1', 'val2', 'val3'])
#gung = SecureExchange(['goob', 'boog', 'garbo'])