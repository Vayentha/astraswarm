import socket
import asyncio

HOST = ''  # The server's hostname or IP address
PORT = 8000        # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(b'GET')
    data = s.recv(1024)
