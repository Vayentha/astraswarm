import sys
import os
import hashlib as hl
import numpy as np
import mwdcharacters.characters
from dataparser import parse as ps


def cyanogen():
    array = np.array([])
    return 1


def get_secure_hash(message):
    m = hl.sha3_512()
    m.update(b"{message}\n")
    return m.hexdigest()


def print_hdg(message):
    m = hl.sha3_512()
    m.update(b"{message}\n")
    print(m.hexdigest())


# new line stub for testing purposes
def nlstub():
    print("\n---------------\n")





def cyanogen_test_import():
    return "Cyanogen Module loaded properly!"


class Rune:
    def __init__(self, rt, data):
        self.runetype = rt
        self.data=data
        self.get_runic_hash()
        self.linked_runes = []
        self.hashlist = []

    def get_rune(self):
        print(f"rune type is : {self.runetype}\n")
        print(f"rune data is : {self.data}\n")
        print(f"rune hash is : {self.runic_hash}\n")
        print(f"linked runes are : {self.linked_runes}")
        print(f"linked hashed are : {self.hashlist}")



    def print_runes(self):
        nlstub()
        print("Overview of Runes:\n")
        if self.check_valid_rune() == -1:
            print("invalid rune.")
        else:
            self.get_rune()
        nlstub()

    def get_runic_hash(self):
        self.runic_hash = get_secure_hash(self.data)
        # print_hdg(self.runic_hash)

    def check_valid_rune(self):
        if self.runetype == "" or self.runetype == "blank":
            return -1
        else:
            if self.runetype == "Sorcery":
                return 1
            if self.runetype == "Alchemy":
                return 2

    def link_rune(self, rune_t):
        if rune_t.check_valid_rune() == -1:
            return
        else:
            self.linked_runes.append(rune_t)
            #print(rune_t.get_runic_hash())
            #hash = rune_t.get_runic_hash()
            #self.hashlist.append(hash)
            return

    def append_hash(self, hash):
        self.hashlist.append(hash)









class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class SLinkedList:
    def __init__(self):
        self.headval = None


    def AtEnd(self, newdata):
        NewNode = Node(newdata)
        if self.headval is None:
            self.headval = NewNode
            return
        laste = self.headval
        while(laste.nextval):
            laste = laste.nextval
        laste.nextval=NewNode


    def LListprint(self):
        printval = self.headval
        while (printval):
            print(printval.data),
            printval = printval.next

# ============TESTCASES ===
#rgb = Rune("Sorcery", 3)
#rgb.print_runes()
#rgb.get_runic_hash()

#add = Rune("Alchemy", 7)

#ghash = add.get_runic_hash()


#rgb.link_rune(add)
#rgb.append_hash(ghash)
#rgb.print_runes()
#llist = SLinkedList()
#llist.Atbegining("Mon")
#llist.Atbegining("Tue")
#llist.Atbegining("Wed")
#llist.Atbegining("Thu")
#llist.RemoveNode("Tue")
#llist.LListprint()

# pretty formatter

# ==============================


def pfo():
    print("************************")


def hydra(content):
    pfo()
    print(f"*|   {content}  ")
    print("*|")


def ali(content):
    print(f"*|   {content}")


def set_hydra_contents():
    return



# tests
vg = ps.SecureExchange(['volume1','volume2','volume3'])

hydra(vg.name)
ali("runes: ")
ali(vg.vkey.hash1.hexdigest())
ali(vg.vkey.hash2.hexdigest())
ali(vg.vkey.hash3.hexdigest())
pfo()
