import argparse
import socketserver
import time
import asyncio
import http.server
import threading


async def check_connections():
    print("milcom endpoint.")

async def main():
    await check_connections()
    await server_run()

async def server_run():
    handler = http.server.SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("", 8000), handler)
    print("serving on port 8000")
    httpd.serve_forever()



asyncio.run(main())