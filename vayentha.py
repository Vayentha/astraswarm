import numpy as np
import hashlib as hl
import sqlite3

def mix_hash():
    return

def remix_hash():
    return

def oss_stub():
    print("---------------")
    print("  O.S.S Cert   ")
    print("---------------")

class Octane:
    # version = "0.1.1" # TODO add semver capabilities
    def __init__(self):
        self.data = []
        self.packages = []
        self.version = "0.1.1"
        self.manu_code = ""

    def add_package(self, package_name):
        if package_name is None or package_name == "":
            return
        self.packages.append(package_name)

    def get_version(self):
        print(self.version)

    def gen_manufacturer_code(self):
        code = np.random.random(size=7)
        self.manu_code = code
        print(self.manu_code)

    def get_packages(self):
        print(self.packages)

    def check_build(self):
        for pkg in self.packages:
            print(pkg)




#oct = Octane()

#oct.gen_manufacturer_code()

#oct.get_packages()

#oct.add_package(package_name="vayentha@0.1.1")


conn = sqlite3.connect('alpha.db')

c = conn.cursor()

#c.execute('''CREATE TABLE stocks
#             (date text, trans text, symbol text, qty real, price real)''')
#c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
print("written to db")
t = ('RHAT',)
c.execute('SELECT * FROM stocks WHERE symbol=?', t)
print(c.fetchone())

# Larger example that inserts many records at a time
purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             ('2006-04-06', 'SELL', 'IBM', 500, 53.00),
            ]
c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases)
conn.close()

